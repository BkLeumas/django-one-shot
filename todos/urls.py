from django.urls import path
from .views import Todolist
from .views import Todo_list_detail

urlpatterns = [
    path("", Todolist, name="todo_list_list"),
    path("<int:id>/", Todo_list_detail, name="todo_list_detail "),
    path

]
